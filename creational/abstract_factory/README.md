# Abstract factory

Permite criar variações de objetos. Ou seja, todos os objetos têm caracteristicas fundamentais que os identificam com um detemrinado tipo de objetos e outras não tão importantes que permitem definir, por exemplo, um modelo do objeto.
O que este padrão permite é a criação de obejtos por modelos/caracteristicas não fundamentais. 
Neste caso, teremos a definição das caracteristicas fundamentais dos objetos, ou seja, as suas funções (métdos/ funcionalidades).
Depois, o abstract factory permitirá que subclasses criem os obejtos acima referidos, mas cada subclasse tem a sua caracteristica identitária (seja pelo deseing, aparência, cor, etc...)

**Baseado em: **https://refactoring.guru/design-patterns/abstract-factory
