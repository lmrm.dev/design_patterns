package products;

/**
 * Computer
 */
public abstract class Computer {

    protected boolean isOn = false;
    protected int volume = 0;
    protected int amtRAM;
    protected int amtInternalStorage;
    protected int processorSpeedLevel;

    public abstract void turnOn();
    public abstract void turnOff();
    public abstract void increaseVolume();
    public abstract void decreaseVolume();

}