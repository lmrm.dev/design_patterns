package products;

public class LeisureComputer extends Computer {

    @Override
    public void turnOn() {
        this.isOn = true;
        System.out.println("Turns on in 20 seconds.");
    }

    @Override
    public void turnOff() {
        this.isOn = false;
    }

    @Override
    public void increaseVolume() {
        this.volume = this.volume+1;
    }

    @Override
    public void decreaseVolume() {
        this.volume = this.volume-1;
    }
    
}