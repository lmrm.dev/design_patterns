package products;

public class WorkComputer extends Computer {

    @Override
    public void turnOn() {
        this.isOn = true;
        System.out.println("Turns on in 5 seconds.");
    }

    @Override
    public void turnOff() {
        this.isOn = false;
    }

    @Override
    public void increaseVolume() {
        this.volume = this.volume+1;
    }

    @Override
    public void decreaseVolume() {
        this.volume = this.volume-1;
    }
    
}
