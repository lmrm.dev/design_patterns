import java.util.Scanner;

import factories.GamingFactory;
import factories.LeisureFactory;
import factories.SetupFactory;
import factories.WorkFactory;

public class App {
    public static void main(String[] args) throws Exception {
        SetupFactory factory;
        Scanner s = new Scanner(System.in);
        System.out.println("What type of setup do you want to build?\n1-Leisure\n2-Work\n3-Gaming\n");
        int i = s.nextInt();
        s.close();
        switch (i) {
            case 1:
                factory = new LeisureFactory();
                break;
            case 2:
                factory = new WorkFactory();
                break;
            case 3:
                factory = new GamingFactory();
                break;
            default:
                return;
        }

        factory.createComputer().turnOn();
        factory.createMouse().scroll();
        
    }
}
