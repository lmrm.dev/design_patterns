package factories;

import products.Computer;
import products.Mouse;

public abstract class SetupFactory {
    public abstract Computer createComputer();
    public abstract Mouse createMouse();
}
