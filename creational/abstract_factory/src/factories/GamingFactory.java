package factories;

import products.Computer;
import products.GamingComputer;
import products.GamingMaouse;
import products.Mouse;

public class GamingFactory extends SetupFactory {

    @Override
    public Computer createComputer() {
        return new GamingComputer();
    }

    @Override
    public Mouse createMouse() {
        return new GamingMaouse();
    }

    
}
