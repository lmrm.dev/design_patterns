package factories;

import products.Computer;
import products.Mouse;
import products.WorkComputer;
import products.WorkMouse;

public class WorkFactory extends SetupFactory {

    @Override
    public Computer createComputer() {
        return new WorkComputer();
    }

    @Override
    public Mouse createMouse() {
        return new WorkMouse();
    }

    
}