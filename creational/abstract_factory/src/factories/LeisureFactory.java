package factories;

import products.Computer;
import products.LeisureComputer;
import products.LeisureMouse;
import products.Mouse;

public class LeisureFactory extends SetupFactory {

    @Override
    public Computer createComputer() {
        return new LeisureComputer();
    }

    @Override
    public Mouse createMouse() {
        return new LeisureMouse();
    }

    
}
