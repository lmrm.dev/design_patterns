package creators;

import products.DeliveryByTruck;
import products.DeliveryLogistics;

public class TruckDeliverySystem extends DeliverySystem {

    @Override
    public DeliveryLogistics despatch(String deliveryId, String tranportId, String from, String to, double distance) {
        return new DeliveryByTruck(deliveryId, tranportId, from, to, distance);  
    }
    
}
