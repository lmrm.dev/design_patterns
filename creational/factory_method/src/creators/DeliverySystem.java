package creators;

import products.DeliveryLogistics;

public abstract class DeliverySystem {

    public abstract DeliveryLogistics despatch(String deliveryId, String tranportId, String from, String to, double distance);
    
    public void deliver(String deliveryId, String tranportId, String from, String to, double distance) { 
        DeliveryLogistics dl = despatch(deliveryId, tranportId, from, to, distance);
        dl.deliver();
    };
}
