package creators;

import products.DeliveryBySea;
import products.DeliveryLogistics;

public class SeaDeliverySystem extends DeliverySystem {

    @Override
    public DeliveryLogistics despatch(String deliveryId, String tranportId, String from, String to, double distance) {
        return new DeliveryBySea(deliveryId, tranportId, from, to, distance);  
    }

    
}
