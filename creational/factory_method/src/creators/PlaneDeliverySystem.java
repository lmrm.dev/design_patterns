package creators;

import products.DeliveryByPlane;
import products.DeliveryLogistics;

public class PlaneDeliverySystem extends DeliverySystem {

    @Override
    public DeliveryLogistics despatch(String deliveryId, String tranportId, String from, String to, double distance) {
        return new DeliveryByPlane(deliveryId, tranportId, from, to, distance);  
    }
    
}