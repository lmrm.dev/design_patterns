package products;

public class DeliveryByPlane extends DeliveryLogistics {

    private String planeId;

    public DeliveryByPlane(String deliveryId, String planeId, String from, String to, double distance) {
        this.deliveryId = deliveryId;
        this.planeId = planeId;
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    @Override
    public void deliver() {
        System.out.println("The delivery " + deliveryId + " is being transported from " + from + " to " + to + " by the plain with the id " + planeId);
    }

    @Override
    public double cost() {
        return distance * 50;
    }
    
}