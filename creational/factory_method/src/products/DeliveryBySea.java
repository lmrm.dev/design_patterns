package products;

public class DeliveryBySea extends DeliveryLogistics {

    private String shipId;

    public DeliveryBySea(String deliveryId, String shipId, String from, String to, double distance) {
        this.deliveryId = deliveryId;
        this.shipId = shipId;
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    @Override
    public void deliver() {
        System.out.println("The delivery " + deliveryId + " is being transported from " + from + " to " + to + " by the ship with the id " + shipId);
    }

    @Override
    public double cost() {
        return distance * 25;
    }
    
}
