package products;

public class DeliveryByTruck extends DeliveryLogistics {

    private String truckId;

    public DeliveryByTruck(String deliveryId, String truckId, String from, String to, double distance) {
        this.deliveryId = deliveryId;
        this.truckId = truckId;
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    @Override
    public void deliver() {
        System.out.println("The delivery " + deliveryId + " is being transported from " + from + " to " + to + " by the truck with the id " + truckId);
    }

    @Override
    public double cost() {
        return distance * 70;
    }
    
}
