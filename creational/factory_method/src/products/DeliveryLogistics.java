package products;

public abstract class DeliveryLogistics {
    protected String deliveryId;
    protected String from;
    protected String to;
    protected Double distance;

    public abstract void deliver();
    public abstract double cost();
}
