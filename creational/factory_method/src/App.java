import java.util.Scanner;

import creators.DeliverySystem;
import creators.PlaneDeliverySystem;
import creators.SeaDeliverySystem;
import creators.TruckDeliverySystem;

public class App {

    private static DeliverySystem deliverySystem;
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        System.out.println("Shipment way: \n1 - Sea\n2 - Plane\n3 - truck");
        int value = in.nextInt();
        in.close();
        if(value == 1) {
            deliverySystem = new SeaDeliverySystem();
        } else         if(value == 2) {
            deliverySystem = new PlaneDeliverySystem();
        } else         if(value == 3) {
            deliverySystem = new TruckDeliverySystem();
        }else
            return;
        
        deliverySystem.deliver("12x23re4", "dewoifn239fenu", "Porto", "Vila Real", 12);
    }
}