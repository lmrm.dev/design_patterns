# Factory method
A criação dos objetos é feita numa superclasse, no entanto, a especialização será feita nas subclasses.
Na superclasse será criada um factory mehtod responsável por criar produtos. Este é um método abstrato e por isso não tem implementação. Essa será feita nas sub classesda classe onde se insere, através do override da mesma.
O conteudo de cada overrude será relativamente simples, dado que terá apenas um return para a criação de uma nova instância do sub objeto chamado produto.
A criação deste padrão passa por dois pontos:
- A criação de objetos (produtos). Estes devem implementar a interface com as funções gerais;
- A criação das classes criadoras dos objetos acima referidos tambem devem ter uma suerclasse em comum. Cada uma destas classes deve impementar o factory method.
Este padrão permitirá a implementação de código geral na superclasse, sendo apenas depois especificado aquando da criação da instância da classe criadora (que por sua vez indicará o respetico produto que deverá ser criado).

**Baseado em**: https://refactoring.guru/design-patterns/factory-method

