# Prototype

Permite a criação de uma cópia de um objeto sem que os métodos privados atrapalhem a operação.
O responsável pelo processo de clonagem é o p´roprio objeto através da criação de uma interface.

**Baseado em:** https://refactoring.guru/design-patterns/prototype
