# Singleton

Permite que uma determinada classe tenha apenas uma instancia durante o runtime da aplicação ao permitir um ponto de acesso publico geral.
O facto de ter apenas uma isntancia permite que não haja conflito de acessos a recursos partilhados.
A sua criação passa por fazer o construtor privado e criar um método público que retorna uma isntancia já existente numa propriedade que guarde a instancia da classe, ou então, retorne uma nova instância.

**Based on:** https://refactoring.guru/design-patterns/singleton
