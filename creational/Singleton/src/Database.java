/**
 * Database
 */
public class Database {

    private static Database instance;
    public String value;

    public static Database getInstance(String value) {
        if(instance== null)
            instance = new Database(value);
        return instance;
    }

    private Database(String value) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        this.value = value;
    }
    
    
}