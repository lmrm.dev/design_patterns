# Builder

Permite cosntruir um objeto composto através de diferentes fases sendo qua cada fase tem componentes diferentes (a funcionalidade é a mesma, as caracteristicas é que variam).
Isto permite evitar a criação de métodos construtores gerais excessivamente grandespor conterem todos as caracteristicas que compõem cada um dos objetos que formam o o objeto geral.
A criação de um diretor permite definir a ordem de criação de objetos.
A classe director é composta pelos métodos que farão a distinção do processo de criação dos objetos finais distintos. Cada uma destas funções chama um buolder que define as caracteristicas próprias de cada objeto. Cada builder contreto setá na sua posse um objeto da classe de obejtos que representa.
basicamente, cada builder serve como setter desse objeto. O builder é passado para o director atravé sdo argumento dos seus métodos.

**Baseado em:** https://refactoring.guru/design-patterns/builder

