package products;


/**
 * Car is a product class.
 */
public class Car {
    private final int carType;
    private final int seats;
    private final int engine;
    private final String transmission;
    private final String tripComputer;
    private final String gpsNavigator;
    private double fuel = 0;

    public Car(int carType, int seats, int engine, String transmission,
               String tripComputer, String gpsNavigator) {
        this.carType = carType;
        this.seats = seats;
        this.engine = engine;
        this.transmission = transmission;
        this.tripComputer = tripComputer;
        this.gpsNavigator = gpsNavigator;
    }

    public int getCarType() {
        return carType;
    }

    public double getFuel() {
        return fuel;
    }

    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    public int getSeats() {
        return seats;
    }

    public int getEngine() {
        return engine;
    }

    public String getTransmission() {
        return transmission;
    }

    public String getTripComputer() {
        return tripComputer;
    }

    public String getGpsNavigator() {
        return gpsNavigator;
    }
}