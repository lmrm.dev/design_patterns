package director;

import builders.Builder;

public class Director {

    public void constructSportsCar(Builder builder) {
        builder.setCarType(1);
        builder.setSeats(2);
        builder.setEngine(3);
        builder.setTransmission();
        builder.setTripComputer();
        builder.setGPS();
    }

    public void constructCityCar(Builder builder) {
        builder.setCarType(2);
        builder.setSeats(2);
        builder.setEngine(1);
        builder.setTransmission();
        builder.setTripComputer();
        builder.setGPS();
    }

    public void constructSUV(Builder builder) {
        builder.setCarType(3);
        builder.setSeats(4);
        builder.setEngine(2);
        builder.setTransmission();
        builder.setGPS();
    }
}
