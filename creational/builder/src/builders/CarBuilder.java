package builders;

import products.Car;

public class CarBuilder extends Builder {
    private int type;
    private int seats;
    private int engine;
    private String transmission;
    private String tripComputer;
    private String gpsNavigator;


    @Override
    public void setCarType(int type) {
        this.type = type;
        
    }

    @Override
    public void setSeats(int number) {
        this.seats = number;
        
    }

    @Override
    public void setEngine(int number) {
        this.engine = number;
        
    }

    @Override
    public void setTripComputer() {
        this.tripComputer = "Car";
        
    }

    @Override
    public void setTransmission() {
        this.transmission = "Car";
        
    }

    @Override
    public void setGPS() {
        this.gpsNavigator = "Car";
        
    }

    public Car getResult() {
        return new Car(type, seats, engine, transmission, tripComputer, gpsNavigator);
    }


}
