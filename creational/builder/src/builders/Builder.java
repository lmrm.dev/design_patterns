package builders;

public abstract class Builder {
    public abstract void setCarType(int type);
    public abstract void setSeats(int number);
    public abstract void setEngine(int number);
    public abstract void setTripComputer();
    public abstract void setTransmission();
    public abstract void setGPS();
}
