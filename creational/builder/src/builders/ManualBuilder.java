package builders;

import products.Manual;

public class ManualBuilder extends Builder {
    private int type;
    private int seats;
    private int engine;
    private String transmission;
    private String tripComputer;
    private String gpsNavigator;


    @Override
    public void setCarType(int type) {
        this.type = type;
        
    }

    @Override
    public void setSeats(int number) {
        this.seats = number;
        
    }

    @Override
    public void setEngine(int number) {
        this.engine = number;
        
    }

    @Override
    public void setTripComputer() {
        this.tripComputer = "Manual";
        
    }

    @Override
    public void setTransmission() {
        this.transmission = "Manual";
        
    }

    @Override
    public void setGPS() {
        this.gpsNavigator = "Manual";
        
    }

    public Manual getResult() {
        return new Manual(type, seats, engine, transmission, tripComputer, gpsNavigator);
    }

}
